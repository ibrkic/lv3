﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationManager notificationManager = new NotificationManager();
            NotificationBuilder builder = new NotificationBuilder();
            NotificationBuilder builder2 = new NotificationBuilder();
            NotificationDirector director = new NotificationDirector(builder);
            director.InfoBuilder("Mike");
            ConsoleNotification consoleNotification = builder.Build();
            notificationManager.Display(consoleNotification);
            ConsoleNotification clone = new ConsoleNotification();
            clone=(ConsoleNotification)consoleNotification.Clone();
            notificationManager.Display(clone);
            clone = (ConsoleNotification)consoleNotification.ShallowClone();
            notificationManager.Display(clone);
        }
    }
}
