﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3
{
    class GeneratorMatrix
    {
        private static GeneratorMatrix instance;
        private RandomGenerator randomGenerator= RandomGenerator.GetInstance();
        private GeneratorMatrix()
        {
        }
        public static GeneratorMatrix GetInstance()
        {
            if (instance == null)
            {
                instance = new GeneratorMatrix();
            }
            return instance;
        }
        public double[][] RandomMatrix(int rows, int columns)
        {
            double[][] matrix = new double[rows][];
            for(int i=0;i<rows;i++)
            {
                matrix[i] = new double[columns];
            }
            for(int i=0;i<rows;i++)
            {
                for(int j=0;j< columns; j++)
                {
                    matrix[i][j] = randomGenerator.NextDouble();
                }
            }
            return matrix;
        }
    }
}
