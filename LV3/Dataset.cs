﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3
{
    class Dataset:Prototype
    {
        private List<List<string>> data; //list of lists of strings
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return
           new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }                public Prototype Clone()
        {
            Dataset clone = new Dataset();
            clone.data = new List<List<string>>();
            foreach(List<string> row in data)
            {
                List<string> rows = new List<string>();
                foreach(string item in row)
                {
                    rows.Add(item);
                }
                clone.data.Add(rows);
            }
            return (Prototype)clone;
        }        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (List<string> row in data)
            {
                foreach (string item in row)
                {
                    builder.Append(item);
                }
                builder.AppendLine();
            }
            return builder.ToString();
        }
    }
}
