﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3
{
    class NotificationDirector
    {
        IBuilder builder;
        public NotificationDirector(IBuilder builder)
        {
            this.builder = builder;
        }
        public void SetBuilder(IBuilder builder)
        {
            this.builder = builder;
        }

        public IBuilder InfoBuilder(string author)
        {
            return builder.SetAuthor(author).SetColor(ConsoleColor.White).SetLevel(Category.INFO).SetText("Just a random information.").SetTime(DateTime.Now).SetTitle("INFO");
        }
        public IBuilder AlertBuilder(string author)
        {
            return builder.SetAuthor(author).SetColor(ConsoleColor.Red).SetLevel(Category.ALERT).SetText("Just a random alert.").SetTime(DateTime.Now).SetTitle("ALERT");
        }
        public IBuilder ErrorBuilder(string author)
        {
            return builder.SetAuthor(author).SetColor(ConsoleColor.DarkRed).SetLevel(Category.ERROR).SetText("Just a random error.").SetTime(DateTime.Now).SetTitle("ERROR");
        }
    }
}
