﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3
{
    class Logger
    {
        private static Logger instance;
        string filePath;
        private Logger()
        {
            filePath = "C:\\Users\\Ivan\\OneDrive - etfos.hr\\Ivan\\Radna površina\\Studiranje\\RPPOON\\LV3\\LV3\\text.txt";
        }
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }
        public void SetPath(string filePath)
        {
            this.filePath = filePath;
        }

        public void Log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath))
            {
                writer.WriteLine(message);
            }
        }
    }
}
